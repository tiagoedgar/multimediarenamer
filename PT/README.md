## Multimedia Renamer

Olá Mundo!
Este é o meu primeiro programa totalmente de borla! =D

#Faz o download
[MultimediaRenamer-1.0.0.jar](https://bitbucket.org/tiagoedgar/multimediarenamer/downloads/MultimediaRenamer-1.0.0.jar)

# Uso
Apenas ** copia ** o arquivo jar (MultimediaRenamer-1.0.0.jar) ** para o diretório ** que contém imagens e executa-o.
No Windows é apenas um ** duplo clique **.

Também pode ser executado usando a linha de comando onde aparece o log da aplicação:

	java -jar MultimediaRenamer-1.0.0.jar

---
## Introdução

*** Eu tinha um problema ***: *A bateria do meu telemovel estava sempre fraca quando eu mais precisava dele para capturar os momentos importantes.*

Quando estava de férias, fui para um lugar muito distante ... Eu nunca lá tinha estado e não sabia como lá chegar... então usei o GPS do meu telefone.
    
** Problema **: Quando eu finalmente cheguei ao meu destino, eu percebi que é um lugar incrível ([Chaves, Portugal](https://en.wikipedia.org/wiki/Chaves,_Portugal)), com Castelo e uma ponte bonitas, mas ... adivinhem: nenhuma bateria no telefone para tirar fotos :(

** Solução **: ** Comprei uma câmera barata ** que seja suficiente (pelo menos para mim) para tirar algumas fotos. * Nota importante: esta câmera não tem GPS. *

** Novo Problema **: Quando eu chegar em casa depois de minhas férias, eu percebi que a câmera cria os ficheiros com o nome: IMG_001.jpg, IMG_002.jpg ... o que significa que, de certeza, um dia eu vou copiar as fotos meu cartão SD para a pasta no meu computador e, em seguida, ** substituir/esmagar minhas fotos anteriores **!

	Se estás aqui ... e a ler isso ... Tenho certeza que isso já te aconteceu, certo? : P
	Não te preocupes, é por isso que eu criei este software ;)

** Nova Solução **: Este software! =D A ideia é ** renomear fotos e videos ** tirados pela minha câmera (ou pela tua), e renomear os arquivos usando a data em que a foto foi tirada (tal como o Android faz). Usando essa solução, nenhum arquivo terá o mesmo nome e, assim, todas as fotos poderão ser reorganizadas pelo nome (que será a data de captura da foto), misturando fotos do Telefone e da Câmera, e ficando ainda com uma linha temporal correta.

##Casos de uso
** Primeiro cenário **: Vais a um grande evento (Ex: casamento, férias, jogo de futebol) com todos os teus amigos, cada um leva a sua câmera, todos tiram fotos e se divertem. No final, todas as imagens são armazenadas na mesma pasta *na cloud* e o link da pasta é partilhado por todos. Até agora, tudo bem, mas... talvez as fotos tenham formatos de nome diferentes (dependendo de quem a tirou) e talvez, o nome seja do genero: Foto001.jpg.
Usando este software, todas as imagens terão o mesmo formato de nome, que é a data em que a foto foi tirada (de acordo com as configurações da câmera).

** Segundo cenário **: És um foto freelancer, e andas sempe com umas 3 câmeras (mais o smartphone), e tens que cobrir algum evento (Ex: um evento de Team Building de uma empresa, um casamento, ou um jogo de futebol). No final do dia, colocas todas as fotos em uma pasta para editá-las. Agora que tens todas as imagens, percebes que o nome não segue o mesmo padrão e quando organizas por nome, não ficas com uma linha do tempo correta.

** Terceiro cenário **: Estás a ler isso no teu PC e sabes que tens várias fotos e backups em várias pastas diferentes para reorganizar ou excluir fotos duplicadas (que talvez tenham nomes diferentes)... e até pior, tens vários discos esternos onde isto também acontece e estás prestes a comprar outro para armazenar mais fotos, porque simplesmente não tens espaço suficiente ...
Se quiseres ajuda com isso, basta colocar as fotos numa pasta e executar este software. Ele irá renomeá-los, criando uma linha do tempo e excluindo todas as imagens duplicadas (de acordo com a data de criação e o conteúdo).

** Ainda melhor **: Eu uso o Google Photos, e apercebi-me que ao utilizar este software, a Google cruzou informações sobre minha localização GPS do telefone e o nome da imagem (que é uma data) e concluiu que a foto foi tirada em "Chaves" nessa data específica. Obrigado Google =)

---

## Características
1. Renomeia imagens (extensões: jpeg, jpg) e arquivos de vídeo (extensão: mov) usando a data em que a foto / vídeo é tirada como Android.
2. Eliminar arquivos duplicados: Se após a renomeação, já existir uma imagem com o mesmo nome, compara o conteúdo dos dois arquivos. Se o conteúdo for o mesmo, o arquivo será excluído. Se o conteúdo não for o mesmo, tenta renomear com "_0", "_1", etc... no final do nome do ficheiro.
3. Cria um arquivo de log de auditoria (logRename.txt) para rastrear os arquivos renomeados (antes -> depois)
4. Cria um arquivo de log de aplicativo (app.log) que registra toda a saída.
5. Cria um arquivo de log de configuração (log4j.properties) para o log do aplicativo. Útil para alterar o nível de log, se necessário.

---

## Arquivos Relevantes
1. O arquivo jar (software): target \ MultimediaRenamer-1.0.0.jar
2. Arquivo de auditoria: logRename.txt
3. log do aplicativo: app.log
4. Configuração do log do aplicativo: log4j.properties
5. MultimediaRenamer Configuration file: config.properties
---

##Configuration
Para alterar o comportamento por omissão, basta editar o ficheiro de confirações *config.properties*:
O nome final dos ficheiros renomeados tem o formato: Prefix+DateFormat+Suffix

	ValidExtensions=jpeg,jpg,mov  	<-- Ficheiros permitidos a serem renomeados  
	Prefix=							<-- Prefixo para os ficheiros permitidos a serem renomeados
	Suffix=							<-- Sufixo para os ficheiros permitidos a serem renomeados
	DateFormat=yyyy-MM-dd HH.mm.ss  <-- Formato da data para os ficheiros permitidos a serem renomeados
	maxTryRename=1000				<-- Numero máximo de tentativas para renomear um ficheiro em caso de duplicados
---

## Versions
* 0.0.1 - Versão inicial: prova de conceito.
* 0.0.2 - Versão atual: refatoração de código.
* 0.0.3 - Novo: Ficheiro de configuração.
* 0.0.4 - Novo: Resolução automática de conflitos em ficheiros com o mesmo nome, mas que não são iguais em conteudo.
* 1.0.0 - Primeira versão (e provavelvelmente a final). Refactorização de código: Multimedia files agora extende de Java File, dando-lhe mais funcionalidades.
---

## Trabalho futuro
Depende de ti! Se tens alguma ideia para melhorar este software, manda-me um email. Se dá jeito a ti, de certeza que dá jeito a mais alguém (foi assim que isto nasceu e foi partilhado).   

## Sobre mim
Sou Português e gosto de resolver problemas.
Se tens ideias pare melhorar este software, ou só me queres agradecer por ter resolvido também um problema em que te identificas, envia-me um e-mail para: tiagoedgar@gmail.com