## Multimedia Renamer

Hello World! 
This is my fist open source software! =D 

És português? [Então clica aqui!](https://bitbucket.org/tiagoedgar/multimediarenamer/src/master/PT/) 

#Download now
[MultimediaRenamer-1.0.0.jar](https://bitbucket.org/tiagoedgar/multimediarenamer/downloads/MultimediaRenamer-1.0.0.jar)

# Usage
Just **copy** the jar file (MultimediaRenamer-1.0.0.jar) **to the directory** that contains pictures and execute it.
In Windows is just a **double click** on the file.

Can also be executed using the command line with log output: 
	
	java -jar MultimediaRenamer-1.0.0.jar

---
##Introduction

***I had a problem*** : *My phones battery was always low when I needed it most to capture the important moments.*

When I was on vacations, I went to a very far place... I never had been there, and I didn't know how to get there... so I used the GPS on my phone. 
    
**Problem**: When I finally arrived to my destination, I realized that It is a amazing place ([Chaves, Portugal](https://en.wikipedia.org/wiki/Chaves,_Portugal)), with a nice Castle and a beautiful bridge, but... guess what: no phone battery to take photos :(

**Solution**: **Buy a cheap camera** that is enough (at least for me) to take some pictures. *Important note: this camera does not have GPS.*

**New Problem**: When I get home after my vacations, I realized  that the camera creates the files with the name: IMG_001.jpg, IMG_002.jpg... meaning that for sure, one day I will copy the photos from my SD Card to the folder on my computer, and then, **override my previous photos**!

	If you are here... reading this... I'm pretty sure that this already happed to you, right? :P
	Dont worry, thats why I created this software ;)

**New Solution**: This software! =D The idea is **rename pictures and movies** taken by my camera (or yours), and rename the files using the date when the photo was taken (just as Android phone does). Using this solution, no file will have the same name, and this way, all photos can be rearranged by name (which will be the photo capture date), mixing Phone and Camera photos, and still having a correct timeline.

##Picture this
**First scenario**: You go on a major event (Ex: wedding, vacations, football match) with all your friends, each one uses their camera, everyone takes pictures, and have fun. In the end, all pictures are stored in the same folder, and you share it using a cloud service. So far, so good, but... maybe the pictures have different name formats (depending on who took it) and maybe, the name is just like Foto001.jpg.
By using this, all pictures will have the same name format, which is the date when it was taken (according to the camera settings).

**Second scenario**: You are a photo freelancer, and you have maybe 3 cameras with you (and your smartphone), and you have to cover some event (Ex: an Enterprise Team Building even, a wedding, or a football match). In the end of the day, you put all pictures in a folder to edit. Now that you have all pictures, you realize that the name does not have the pattern, and when you short by name, you do not have a correct timeline. 

**Third scenario**: You are reading this in your PC, and you know that you have several pictures, and backups in several different folders still to rearrange or delete duplicated photos (that maybe have different names)... and even worst, this happens also in your external drives and you are about to buy another one to store more photos, because you just do not have enough space... 
If you want some help with that, just put your pictures in a folder, and run this software. It will rename them, creating a timeline, deleting all duplicated pictures (according to the creation date and content).

**Even better**: I use Google Photos, and I just realized that using this software Google crossed information about my GPS phone location and the picture's name (that is a date) and concludes that the photo was taken in "Chaves" at that specific date. Thank You Google =) 

---

## Features
1. Renames pictures (extensions: jpeg, jpg) and video files (extension: mov) using the date when the picture/video as taken as Android does.
2. Eliminate duplicate files: If after the rename, already exists a picture with the same name, it compares both files content. If the content is the same, the file is deleted. If the content is not the same, it tries to rename with "_0", "_1", etc... in the end of file name.
3. Creates a audit log file (logRename.txt) to track the renamed files (before --> after)
4. Creates a application log file (app.log) that registers all the output.
5. Creates a configuration log file (log4j.properties) for the application log. Useful to change log level if needed.  

---

## Relevant files
1. The jar file (software): target\MultimediaRenamer-1.0.0.jar
2. Audit file: logRename.txt
3. Application log: app.log
4. Application log configuration: log4j.properties
5. MultimediaRenamer Configuration file: config.properties
---

##Configuration
To change the default values, please edit the config.properties file:
The name format is: Prefix+DateFormat+Suffix

	ValidExtensions=jpeg,jpg,mov  	<-- Files allowed to rename
	Prefix=							<-- Prefix for the allowed renamed file
	Suffix=							<-- Suffix for the allowed renamed file
	DateFormat=yyyy-MM-dd HH.mm.ss  <-- Date format for the allowed renamed file
	maxTryRename=1000				<-- Max try number to rename duplicated files (in content)

## Versions
* 0.0.1 - Initial version: Proof of concept.
* 0.0.2 - Code refactoring.
* 0.0.3 - New: Config File. 
* 0.0.4 - New: Auto-resolve conflicts in files with same name, but different in content.
* 1.0.0 - First (and probably final) public version. Code refactory, Multimedia files now extends Java File functionalities

---

## Future Work
Depends on you! Send me an email with your idea to improve this software and I will consider it =)

## About me
I'm from Portugal and i like to solve problems
If you have ideas to improve this software, or just thank me, please send me an email to: tiagoedgar@gmail.com
