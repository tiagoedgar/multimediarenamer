package pt.tiagoedgar.software.MultimediaRenamer;

import java.io.File;
import org.apache.log4j.Logger;
import pt.tiagoedgar.software.MultimediaRenamer.context.FileContext;
import pt.tiagoedgar.software.MultimediaRenamer.context.MultimediaFile;
import pt.tiagoedgar.utils.ConfigUtils;
import pt.tiagoedgar.utils.Constant;
import pt.tiagoedgar.utils.FileUtils;
import pt.tiagoedgar.utils.LoggerUtils;

/**
 * MultiMedia Renamer
 *
 */
public class App{
	private static final Logger LOGGER = Logger.getLogger(App.class);

	public static void main( String[] args ){
		try {
			init();
			File[] listOfFiles = FileUtils.getAllCurrentDirectoryFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				
				MultimediaFile file = new MultimediaFile(listOfFiles[i].getPath()) ;
				String fileName = file.getName();
				
				LOGGER.debug("File: " + fileName);

				if (file.isValidToRename()) {	
					LOGGER.info("File is valid to rename: " + fileName);
					
					if(file.isAlreadyRenamed()) {
						LOGGER.info("File already renamed: " + fileName);
					}else {
						if(file.isDuplicated()) {
							LOGGER.info("File "+file.getName()+" is duplicated. Going to delete it!");
							file.delete();
							String auditEntry = file.getName() + " --> Duplicate File deleted. Original file: " + file.getFutureName();
							AuditLogger.log(auditEntry);
						
						}else {
							LOGGER.debug("File will be renamed: " +file.getName() +" --> " + file.getFutureName());
							boolean successRename = file.rename();
							if(successRename) {
								String auditEntry = fileName + " --> " + file.getFutureName();
								LOGGER.info("File Renamed: " + auditEntry + " | FileContext: " + file);
								AuditLogger.log(auditEntry);									
							}else {
								LOGGER.warn("Problem renaming file: " + file);
							}
						}
					}
				}else {
					LOGGER.debug("File is not valid to rename: " + file);
				}
			}
			LOGGER.info("Done!");

		}catch(Throwable t) {
			LOGGER.error("Application Error: " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t, t);
		}
	}

	private static void init() {
		try {
			LoggerUtils.init();
			LOGGER.info("MultiMedia Init... ");
			//AuditLogger.init();
			//ConfigUtils.init();
		}catch(Throwable t) {
			LOGGER.error("Application Init Error: " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t, t);
		}
	}
}
