package pt.tiagoedgar.software.MultimediaRenamer;

import java.io.File;
import java.util.Date;

import org.apache.log4j.Logger;

import pt.tiagoedgar.utils.AppUtils;
import pt.tiagoedgar.utils.FileUtils;

public class AuditLogger {

	private static final Logger LOGGER = Logger.getLogger(AuditLogger.class);
	
	public static final String AUDIT_LOG_FILE = "audit.log";
	public static File auditFile = null; 
	
	public static void init() throws Throwable {
		try {
			String currentDirectory = AppUtils.getAppPath();
			
			auditFile = new File(currentDirectory + "/" + AUDIT_LOG_FILE);

			if(!auditFile.exists()) {
				LOGGER.info("Creating audit log file: " + auditFile);
				auditFile.getParentFile().mkdirs(); 
				auditFile.createNewFile();				
			}
			
			String auditEntry = "### " + new Date() +"###\n";
			FileUtils.append(auditFile, auditEntry);
			
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error on audit logger init."+ error , t);
			throw t;
		}
		
	}
	
	public static File getLoggerFile() throws Throwable {
		try {
			if(auditFile == null) {
				init();
			}
			return auditFile;			
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting audit file."+ error , t);
			throw t;
		}
	}

	public static void log(String auditEntry) throws Throwable {
		try {
			LOGGER.trace("Audit Entry: " + auditEntry);
			File logger = getLoggerFile();
			FileUtils.append(logger, auditEntry+"\n");
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error writing to audit file."+ error , t);
			throw t;
		}
	}
	
}
