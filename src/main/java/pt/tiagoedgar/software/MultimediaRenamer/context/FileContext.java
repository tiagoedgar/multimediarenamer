package pt.tiagoedgar.software.MultimediaRenamer.context;

import java.io.File;
import org.apache.log4j.Logger;

import pt.tiagoedgar.software.MultimediaRenamer.AuditLogger;
import pt.tiagoedgar.utils.FileUtils;


public class FileContext {
	
	private static final Logger LOGGER = Logger.getLogger(FileContext.class);
	
	private File file;
	private String folder;
	private String originalName;
	private String futureName;
	private boolean alreadyRenamed;
	private boolean duplicate;
	private boolean alreadyExistsRenamedFile;
	
	
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	public String getFutureName() {
		return futureName;
	}
	public void setFutureName(String futureName) {
		this.futureName = futureName;
	}
	public boolean isAlreadyRenamed() {
		return alreadyRenamed;
	}
	public void setAlreadyRenamed(boolean alreadyRenamed) {
		this.alreadyRenamed = alreadyRenamed;
	}
	public boolean isDuplicate() {
		return duplicate;
	}
	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}
	
	public boolean isAlreadyExistsRenamedFile() {
		return alreadyExistsRenamedFile;
	}
	public void setAlreadyExistsRenamedFile(boolean alreadyExistsRenamedFile) {
		this.alreadyExistsRenamedFile = alreadyExistsRenamedFile;
	}
	
	
	@Override
	public String toString() {
		return "FileContext [originalName=" + originalName + ", futureName=" + futureName + ", alreadyRenamed="
				+ alreadyRenamed + ", duplicate=" + duplicate + ", alreadyExistsRenamedFile=" + alreadyExistsRenamedFile
				+ "]";
	}
	public FileContext(File file) throws Throwable {
		LOGGER.debug("Creating file context: " + file.getName());
		this.file = file;
		try {
			this.folder = file.getParent();
			this.originalName= file.getName();
			this.futureName = FileUtils.getRenamedFileName(file);
			this.alreadyExistsRenamedFile = FileUtils.alreadyExists(this.futureName);
			this.alreadyRenamed = this.originalName.equals(this.futureName);
			
			if(!alreadyRenamed && this.alreadyExistsRenamedFile) {
				File file2 = new File(this.folder+"/"+this.futureName);
				this.duplicate = FileUtils.areEqualFiles(file, file2);							
			}else {
				this.duplicate = false;
			}
			LOGGER.debug("New "+this);
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error creating file context:"+file.getName()+ error , t);
			throw t;
		}
	}
	
	public void delete() throws Throwable {
		try {
			LOGGER.info("Deleting file: " + this.originalName);
			FileUtils.delete(this.file);
		} catch (Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error deleting file:"+this.originalName+ error , t);
			throw t;
		}
	}
	
	public void rename() throws Throwable {
		try {
			FileUtils.rename(this.file, this.futureName);
			String auditEntry = this.getOriginalName() + " --> " + this.getFutureName();
			LOGGER.info("File Renamed: " + auditEntry + " | FileContext: " + file);
			AuditLogger.log(auditEntry);
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error renaming file:"+this+ error , t);
			throw t;
		}		
	}
	public void rename(String possibleFutureName) throws Throwable {
		try {
			this.setFutureName(possibleFutureName);
			this.rename();			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error renaming file to "+possibleFutureName+":"+this+ error , t);
			throw t;
		}
	}
	
	public String getFutureName(int tryNumber) throws Throwable {
		String name = "";
		try {
			name = this.futureName;
			String extension = FileUtils.getExtension(this.file);
			name = name.replace("."+extension, "_"+tryNumber+"."+extension);
			return name;			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error simulating future name: "+name+":"+this+ error , t);
			throw t;
		}
	}
	

}
