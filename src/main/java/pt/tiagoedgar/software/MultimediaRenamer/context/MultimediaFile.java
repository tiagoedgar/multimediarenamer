package pt.tiagoedgar.software.MultimediaRenamer.context;

import java.io.File;
import java.io.NotActiveException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import pt.tiagoedgar.utils.ConfigUtils;
import pt.tiagoedgar.utils.Constant;
import pt.tiagoedgar.utils.FileUtils;

public class MultimediaFile extends File{

	private static final Logger LOGGER = Logger.getLogger(MultimediaFile.class);
	private static final long serialVersionUID = 1929852016774243249L;
	
	private static Set<String> validExtensions = null;
		
	private Boolean isValidToRename=null;
	private Boolean isAlreadyRenamed=null;
	private Boolean isDuplicated=null;
	private String futureName = null;
	
	
	@Override
	public String toString() {
		return "MultimediaFile: "+this.getName()+" [isValidToRename=" + isValidToRename + ", isAlreadyRenamed=" + isAlreadyRenamed
				+ ", isDuplicated=" + isDuplicated + ", futureName=" + futureName + "]";
	}

	public MultimediaFile(String pathname) throws Throwable {
		super(pathname);
		if(validExtensions == null) {
			initValidExtensions();
		}
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof MultimediaFile))return false;
	    MultimediaFile otherMyClass = (MultimediaFile)other;
	    if(!this.getName().equals(otherMyClass.getName())) return false;
	    try {
			if(!this.sameContent(otherMyClass.getName())) return false;
			//All checked
			return true;
		} catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if files are equals is valid for rename: "+this.getName()+ error , t);
			return false;
		}
	}
	
	/**
	 * Loads the valid extensions configured.
	 * @throws Throwable
	 */
	private static void initValidExtensions() throws Throwable {
		String validExtensionsString = "";
		try {
			validExtensions = new HashSet<String>(0);
			validExtensionsString = ConfigUtils.getProperty(Constant.CONFIG_VALID_EXTENSIONS, Constant.CONFIG_VALID_EXTENSIONS_DEFAULT_VALUE);
			
			String extensions[] = validExtensionsString.split(Constant.SEPERATOR);
			for(int i=0; i<extensions.length; i++) {
				validExtensions.add(extensions[i]);
			}
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error loading validExtensions: "+validExtensionsString+ error , t);
			throw t;
		}
	}
	
	/**
	 * Check if the current file is valid to be renamed, using the file extension.
	 * @return
	 * @throws Throwable
	 */
	private boolean initIsValidToRename() throws Throwable {
		boolean isValid = false;
		try {
			LOGGER.trace("Checking if file is valid for rename: " + this.getName());
			if(this.isFile()) {
				LOGGER.trace("Checking if file extension is valid for rename: " + this.getName());
				String extension = this.getExtension();
				if(validExtensions.contains(extension)) {
					this.setValidToRename(true);
				}else {
					this.setValidToRename(false);
				}
			}else {
				LOGGER.trace("Cannot rename directory: " + this.getName());
				this.setValidToRename(false);
			}
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file is valid for rename: "+this.getName()+ error , t);
			throw t;
		}
		LOGGER.trace("File is valid for rename ? "+isValid);
		return isValid;
	}


	public Boolean isValidToRename() throws Throwable {
		if(this.isValidToRename==null) {
			initIsValidToRename();
		}
		return isValidToRename;
	}

	private void setValidToRename(Boolean isValidToRename) {
		this.isValidToRename = isValidToRename;
	}


	
	private void initIsAlreadyRenamed() throws Throwable {
		try { 
			String renamedName = this.getFutureName();
			this.setAlreadyRenamed(this.getName().equalsIgnoreCase(renamedName));
			LOGGER.trace("Alredy Renamed ? " + this.isAlreadyRenamed);
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if already renamed: "+this.getName()+ error , t);
			throw t;
		}
	}
	
	public Boolean isAlreadyRenamed() throws Throwable {
		if(isAlreadyRenamed==null) {
			initIsAlreadyRenamed();
		}
		return isAlreadyRenamed;
	}
	
	private void setAlreadyRenamed(Boolean isAlreadyRenamed) {
		this.isAlreadyRenamed = isAlreadyRenamed;
	}
	
	
	
	private void initDuplicate() throws Throwable {
		try {
			LOGGER.trace("init Duplicate: " + this.getName());
			
			if(!this.getName().equals(this.getFutureName())) {
				LOGGER.trace("Checking already exists a file with the name: "+this.getFutureName());
				
				if(this.alreadyExistsRenamed(this.getFutureName())){
					LOGGER.trace("Already exists a renamed file: " + this.getFutureName());
					
					if(this.sameContent(this.getFutureName())){
						LOGGER.trace("The file "+this.getName()+" is equal in content: " + this.getFutureName());
						this.setDuplicated(true);
					}else {
						LOGGER.trace("The file "+this.getName()+" is diferent in content: " + this.getFutureName());
						this.setDuplicated(false);
					}
					
				}else {
					LOGGER.trace("The are no file with the future name. Not duplicate");
					this.setDuplicated(false);
				}
			}else {
				LOGGER.trace("This file name have the same name of the future file name. File already renamed, not duplicate");
				this.setDuplicated(false);
			}
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error on init isDoplicate: "+this.getName()+ error , t);
			throw t;
		}
		
	}

	public Boolean isDuplicated() throws Throwable {
		if(isDuplicated == null) {
			initDuplicate();
		}
		return isDuplicated;
	}

	private void setDuplicated(Boolean isDuplicated) {
		this.isDuplicated = isDuplicated;
	}

	
	public void initFutureName() throws Throwable {
		String newFileName = null;
		try {
			newFileName = this.baseFutureName();
			//Check if available
			int tryNumber;
			int maxTries = ConfigUtils.getPropertyAsInt(Constant.CONFIG_MAX_TRY_RENAME, 1000);
			for(tryNumber=0; tryNumber<maxTries; tryNumber++) {
				String possibleFutureName = newFileName;
				if(tryNumber > 0) {
					String extension = this.getExtension();
					possibleFutureName = possibleFutureName.replace("."+extension, "_"+tryNumber+"."+extension);
				}
				if(this.getName().equals(possibleFutureName)) {
					LOGGER.debug("This file is already renamed: " + possibleFutureName);
					this.setFutureName(possibleFutureName);
					//this.setAlreadyRenamed(true);
					//this.setDuplicated(false);
					break;
				}else {	
					if(this.alreadyExistsRenamed(possibleFutureName)) {
						LOGGER.debug("Already exists a file with the name: " + possibleFutureName);
						if(this.sameContent(possibleFutureName)) {
							LOGGER.debug("Files are equal. File: " + this.getName() + " equal: " + possibleFutureName);
							this.setFutureName(possibleFutureName);
							//this.setDuplicated(true);
							break;
						}else {
							LOGGER.debug("Already exists a diferent file with the name: " + possibleFutureName + " | Check next index");
						}
						
					}else {
						LOGGER.debug("Filename available: " + possibleFutureName);
						this.setFutureName(possibleFutureName);
						break;
					}
				}
			}
			
			if(tryNumber == maxTries) {
				LOGGER.warn("Future name Limit exceded: " + this);
			}
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting renamed name of file "+this.getName()+ error , t);
			throw t;
		}
	}
	
	private boolean sameContent(String possibleFutureName) throws Throwable {
		try {
			String possibleFilePath = this.getFuturePath(possibleFutureName);
			boolean duplicate = FileUtils.areEqualFiles(this.getPath(), possibleFilePath);
			LOGGER.trace("Is duplicate ? " + duplicate + " | This: " + this.getName() + " --> " + possibleFutureName);
			return duplicate;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting renamed name of file "+this.getName()+ error , t);
			throw t;
		}
	}

	private boolean alreadyExistsRenamed(String possibleFutureName) throws Throwable {
		try {
			String possibleFilePath = this.getFuturePath(possibleFutureName);
			boolean alreadyExistsFile = FileUtils.alreadyExists(possibleFilePath);
			LOGGER.trace("Already Exists Renamed ? " + alreadyExistsFile);
			return alreadyExistsFile;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file already exists: "+possibleFutureName+ error , t);
			throw t;
		}
	}

	private String baseFutureName() throws Throwable {
		String baseFileName = null;
		try {
			String prefix=ConfigUtils.getProperty(Constant.CONFIG_PREFIX, Constant.CONFIG_PREFIX_DEFAULT_VALUE);
			String creationDate = this.getCreationDateAsString();
			String sufix =ConfigUtils.getProperty(Constant.CONFIG_SUFFIX, Constant.CONFIG_SUFFIX_DEFAULT_VALUE);
			String extension = this.getExtension();
			baseFileName = prefix + creationDate + sufix + "." +extension;
			LOGGER.trace("Base File Name is: " + baseFileName);
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting renamed name of file "+this.getName()+ error , t);
			throw t;
		}
		return baseFileName;
	}
	/*
	private boolean alreadyExistsRenamed() throws Throwable {
		LOGGER.trace("Already Exists: " + this.getName());
		try {
			String futureName = this.getFutureName();
			File file = new File(futureName);
			return file.exists();
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file already exists: "+this.getName()+ error , t);
			throw t;
		}
	}
	*/
	
	public String getFutureName() throws Throwable {
		if(futureName == null) {
			initFutureName();
		}
		return this.futureName;
	}
	
	
	public void setFutureName(String futureName) {
		this.futureName = futureName;
	}
	
	/*
	private String getFutureName(int tryNumber) throws Throwable {
		String name = "";
		try {
			name = this.futureName;
			String extension = this.getExtension();
			name = name.replace("."+extension, "_"+tryNumber+"."+extension);
			return name;			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error simulating future name: "+name+":"+this+ error , t);
			throw t;
		}
	}
	*/
	
	private String getFuturePath(String possibleFutureName) throws Throwable {
		try {
			//String futurePath = this.getParent()+"/"+this.getFutureName();
			String futurePath = this.getParent()+"/"+possibleFutureName;
			return futurePath;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting future path"+ error , t);
			throw t;
		}
	}

	/*
	private boolean extensionValid() throws Throwable {
		boolean isValid = false;
		try {
			
	
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file extension is valid: "+ error , t);
			throw t;
		}	
		return isValid;
	}
	*/
	
	/**
	 * Retrieves the file extension
	 * @return
	 * @throws Throwable
	 */
	public String getExtension() throws Throwable {
		String extension = null;
		try {
			LOGGER.trace("Getting file extension: " + this.getName());
			extension = FilenameUtils.getExtension(this.getName()).toLowerCase(); // returns "jpg";
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting file extension: "+this.getName()+ error , t);
			throw t;
		}
		
		return extension;
	}

	/**
	 * Retrieves the file creation date as string
	 * @return
	 * @throws Throwable
	 */
	public String getCreationDateAsString() throws Throwable {
		try {
			String dateCreated = null;
			LOGGER.trace("Getting creation date of file: " + this.getName());
			
			BasicFileAttributes fileAttibutes = Files.readAttributes(this.toPath(), BasicFileAttributes.class);
			
			//FIXME review why creationTime() is not the solution
			FileTime creationDateAttribute = fileAttibutes.lastModifiedTime();
			
			String dateFormatString = ConfigUtils.getProperty(Constant.CONFIG_DATE_FORMAT, Constant.CONFIG_DATE_FORMAT_DEFAULT_VALUE);
			DateFormat dateFormat = new SimpleDateFormat(dateFormatString); //2018-05-12 17.00.57.jpg
			dateCreated = dateFormat.format(creationDateAttribute.toMillis());
			
			LOGGER.trace("File: " + this.getName() + " | Creation Date: " + dateCreated);		
		
			return dateCreated;
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting Creation Date of file: "+this.getName()+ error , t);
			throw t;
		}
	}

	public boolean rename() throws Throwable {
		try {
			LOGGER.trace("Renaming file:" + this.getName() + " to: " + this.futureName);
			String newFilePath = this.getParent()+"/"+this.getFutureName();
			File newFile = new File(newFilePath);
			boolean renamed = this.renameTo(newFile);
			LOGGER.trace("File Renamed ? " + renamed);
			return renamed;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error renaming file:"+this+ error , t);
			throw t;
		}		
	}

}
