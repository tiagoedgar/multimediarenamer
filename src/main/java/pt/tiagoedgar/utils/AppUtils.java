package pt.tiagoedgar.utils;

import org.apache.log4j.Logger;

public class AppUtils {
	
	private static final Logger LOGGER = Logger.getLogger(LoggerUtils.class);
	
	private static String CURRENT_DIRECTORY = null; 
	
	private static void init() throws Throwable {
		try {
			LOGGER.trace("Init: Getting application path");
			CURRENT_DIRECTORY = System.getProperty("user.dir");
			LOGGER.debug("Working Directory: " + CURRENT_DIRECTORY );
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error on init getting application path"+ error , t);
			throw t;
		}
	}
	
	public static String getAppPath() throws Throwable {
		try {
			if(JavaUtils.isEmptyOrNull(CURRENT_DIRECTORY)) {
				init();
			}
			return CURRENT_DIRECTORY;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting application path"+ error , t);
			throw t;
		}
		
	}

}
