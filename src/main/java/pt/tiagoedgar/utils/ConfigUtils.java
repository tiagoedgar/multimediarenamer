package pt.tiagoedgar.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigUtils {
	
	private static final Logger LOGGER = Logger.getLogger(ConfigUtils.class);
	
	private static final String CONFIG_FILE = "config.properties";
	private static Properties properties = null;
	
	public static void init() throws Throwable {
		try {
			Properties defaultProperties = getDefaultProperties();

			String currentDirectory = AppUtils.getAppPath();
			String configFilePath = currentDirectory + "/" + CONFIG_FILE;
			File configFile = new File(configFilePath);

			boolean configFileExists = configFile.exists();
			if(!configFileExists) {
				LOGGER.info("Creating default config file: " + CONFIG_FILE);
				configFile.getParentFile().mkdirs(); 
				configFile.createNewFile();						
			}
			
			Properties fileProperties = new Properties();
			FileInputStream in = new FileInputStream(configFilePath);
			fileProperties.load(in);
			in.close();
			
			/*
			 * Works... but does not save the defaults in the config.properties file
			 * properties = new Properties(defaultProperties);
			 */
			properties = new Properties();
			properties.putAll(defaultProperties);
			properties.putAll(fileProperties);
			
			//Create Default file
			FileOutputStream out = new FileOutputStream(configFilePath);
			String comment = "Multimedia Renamer config file";
			properties.store(out, comment);
			out.close();				
			
			LOGGER.debug("Properties Loaded. " + properties);
			
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error on config init."+ error , t);
			throw t;
		}
		
	}
	
	

	private static Properties getDefaultProperties() throws Throwable {
		try {
			LOGGER.debug("Loading default properties");
			Properties defaultProperties = new Properties();
			defaultProperties.setProperty(Constant.CONFIG_PREFIX, Constant.CONFIG_PREFIX_DEFAULT_VALUE);
			defaultProperties.setProperty(Constant.CONFIG_SUFFIX, Constant.CONFIG_SUFFIX_DEFAULT_VALUE);
			defaultProperties.setProperty(Constant.CONFIG_DATE_FORMAT, Constant.CONFIG_DATE_FORMAT_DEFAULT_VALUE);
			defaultProperties.setProperty(Constant.CONFIG_VALID_EXTENSIONS, Constant.CONFIG_VALID_EXTENSIONS_DEFAULT_VALUE);
			defaultProperties.setProperty(Constant.CONFIG_MAX_TRY_RENAME, Constant.CONFIG_MAX_TRY_RENAME_DEFAULT_VALUE);
			
			LOGGER.trace("Default Properties loaded. " + defaultProperties);
			return defaultProperties;
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error on config init."+ error , t);
			throw t;
		}
	}

	public static String getProperty(String name) throws Throwable {
		try {
			if(properties == null) {
				init();
			}
			String value = properties.getProperty(name);
			LOGGER.trace("Property: " + name + "="+value);
			return value;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting property: "+name + error , t);
			throw t;
		}
	}
	
	public static String getProperty(String name, String defaultValue) throws Throwable {
		try {
			String value = getProperty(name);
			if(value == null) {
				LOGGER.warn("Property '"+name+"' not configured. Returning default value: " + defaultValue);
				value = defaultValue;
			}
			return value;			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting property: "+name + error , t);
			throw t;
		}
	}



	public static Integer getPropertyAsInt(String name) throws Throwable {
		Integer valueInt = null;
		try {
			String value = getProperty(name);
			if(value != null) {
				valueInt = Integer.valueOf(value);
			}
			return valueInt;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting property as Int: "+name + error , t);
			throw t;
		}
	}
	
	public static Integer getPropertyAsInt(String name, int defaultValue) throws Throwable {
		try{
			Integer value = getPropertyAsInt(name);
			if(value == null) {
				value = defaultValue;
			}
			return value;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting property as Int: "+name + error , t);
			throw t;
		}
	}

}
