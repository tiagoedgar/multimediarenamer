package pt.tiagoedgar.utils;

public class Constant {
	
	//Configs
	public static final String CONFIG_PREFIX = "Prefix";
	public static final String CONFIG_PREFIX_DEFAULT_VALUE = "";
	
	public static final String CONFIG_SUFFIX = "Suffix";
	public static final String CONFIG_SUFFIX_DEFAULT_VALUE = "";
	
	public static final String CONFIG_DATE_FORMAT = "DateFormat";
	public static final String CONFIG_DATE_FORMAT_DEFAULT_VALUE = "yyyy-MM-dd HH.mm.ss";
	
	public static final String CONFIG_VALID_EXTENSIONS = "ValidExtensions";
	public static final String CONFIG_VALID_EXTENSIONS_DEFAULT_VALUE = "jpeg,jpg,mov";
	
	public static final String CONFIG_MAX_TRY_RENAME = "maxTryRename";
	public static final String CONFIG_MAX_TRY_RENAME_DEFAULT_VALUE = "1000";
	
	
	//Others
	public static final String SEPERATOR = ",";
	
}
