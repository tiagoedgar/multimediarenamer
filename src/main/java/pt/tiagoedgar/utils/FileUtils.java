package pt.tiagoedgar.utils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import org.apache.commons.io.FileUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;

import org.apache.log4j.Logger;


public class FileUtils {
	
	private static final Logger LOGGER = Logger.getLogger(LoggerUtils.class);
	
	private static Set<String> validExtensions = null;

	public static void append(File file, String entry) throws Throwable {
		try {
			Files.write(Paths.get(file.getPath()), entry.getBytes(), StandardOpenOption.APPEND);			
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error appenting file entry to file."+ error , t);
			throw t;
		}
	}
	
	public static String getCreationDateAsString(File file) throws Throwable {
		try {
			String dateCreated = null;
			LOGGER.trace("Getting creation date of file: " + file);
			
			if(file != null) {
				BasicFileAttributes fileAttibutes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
				
				//FIXME review why creationTime() is not the solution
				FileTime creationDateAttribute = fileAttibutes.lastModifiedTime();
				
				String dateFormatString = ConfigUtils.getProperty(Constant.CONFIG_DATE_FORMAT, Constant.CONFIG_DATE_FORMAT_DEFAULT_VALUE);
				DateFormat dateFormat = new SimpleDateFormat(dateFormatString); //2018-05-12 17.00.57.jpg
				dateCreated = dateFormat.format(creationDateAttribute.toMillis());
				
				LOGGER.trace("File: " + file.getName() + " | Creation Date: " + dateCreated);		
			
			}else {
				LOGGER.warn("File does not exists, returning null.");
			}
			
			return dateCreated;
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting Creation Date of file: "+file+ error , t);
			throw t;
		}
		

	}
	
	public static boolean isValidFileForRename(File file) throws Throwable {
		boolean isValid = false;
		try {
			LOGGER.trace("Checking if file is valid for rename: " + file.getName());
			
			if(file != null) {
				if(file.isFile() && extensionValid(file)) {
					isValid = true;
				}
			}
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file is valid for rename: "+file+ error , t);
			throw t;
		}
		LOGGER.trace("File is valid for rename ? "+isValid);
		return isValid;
	}

	private static boolean extensionValid(File file) throws Throwable {
		boolean isValid = false;
		try {
			LOGGER.trace("Checking if file extension is valid for rename: " + file.getName());
			if(file != null) {
				String extension = getExtension(file);
				
				if(validExtensions == null) {
					validExtensions = new HashSet<String>(0);
					String validExtensionsString = ConfigUtils.getProperty(Constant.CONFIG_VALID_EXTENSIONS, Constant.CONFIG_VALID_EXTENSIONS_DEFAULT_VALUE);
					String extensions[] = validExtensionsString.split(Constant.SEPERATOR);
					for(int i=0; i<extensions.length; i++) {
						validExtensions.add(extensions[i]);
					}
				}
				
				if(validExtensions.contains(extension)) {
					isValid = true;
				}
			}
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file extension is valid: "+file+ error , t);
			throw t;
		}
		
		return isValid;
	}
	
	public static String getExtension(File file) throws Throwable {
		String extension = null;
		try {
			LOGGER.trace("Getting file extension: " + file.getName());
			
			if(file != null) {
				extension = FilenameUtils.getExtension(file.getName()).toLowerCase(); // returns "jpg";
			}
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting file extension: "+file+ error , t);
			throw t;
		}
		
		return extension;
	}

	public static void rename(File file, String newName) throws Throwable {
		try {
			if(file != null) {
				LOGGER.trace("Renaming file: " + file.getName() + " to: " + newName);
				String newFilePath = file.getParent()+"/"+newName;
				File dest = new File(newFilePath);
				file.renameTo(dest);
				LOGGER.trace("File renamed!");
			}else {
				LOGGER.warn("No file to rename!");
			}
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error renaming file: "+file+ error , t);
			throw t;
		}
		
	}

	public static boolean alreadyExists(String filePath) throws Throwable {
		LOGGER.trace("Already Exists: " + filePath);
		try {
			File file = new File(filePath);
			return file.exists();
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if file already exists: "+filePath+ error , t);
			throw t;
		}
	}

	public static File[] getAllCurrentDirectoryFiles() throws Throwable {
		try {
			File appFolder = new File(AppUtils.getAppPath());
			File[] listOfFiles = appFolder.listFiles();
			return listOfFiles;
		
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting current directory files"+ error , t);
			throw t;
		}
	}
	
	/**
	 * Returns the new file name string
	 * @param file
	 * @return
	 * @throws Throwable
	 */
	public static String getRenamedFileName(File file) throws Throwable {
		String newFileName = null;
		try {
			String prefix=ConfigUtils.getProperty(Constant.CONFIG_PREFIX, Constant.CONFIG_PREFIX_DEFAULT_VALUE);
			String creationDate = getCreationDateAsString(file);
			String sufix =ConfigUtils.getProperty(Constant.CONFIG_SUFFIX, Constant.CONFIG_SUFFIX_DEFAULT_VALUE);
			String extension = getExtension(file);
			newFileName = prefix + creationDate + sufix + "." +extension;
			LOGGER.trace("Rename File Name is: " + newFileName);
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting renamed name of file "+file+ error , t);
			throw t;
		}
		return newFileName;
	}
	
	
	/**
	 * Returns the complete file path string
	 * @param file
	 * @return
	 * @throws Throwable
	 */
	public static String getRenamedFilePath(File file) throws Throwable {
		String newFileName = null;
		try {
			String path = file.getParent();
			newFileName = path + "/" + getRenamedFileName(file);
			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error getting renamed name of file "+file+ error , t);
			throw t;
		}
		return newFileName;
	}

	public static boolean areEqualFiles(File currentFile, File alreadyRenamedFile) throws Throwable {
		try {
			boolean areEqual = false;
			//TODO Review this latter...
			LOGGER.trace("Comparing files: " + currentFile + " == " + alreadyRenamedFile);
			areEqual = org.apache.commons.io.FileUtils.contentEquals(currentFile, alreadyRenamedFile);			
			LOGGER.trace("Have equal content ? " + areEqual + " | Orig File: " + currentFile.getName() + " Dest File: " + alreadyRenamedFile.getName());
			return areEqual;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error comparing file content"+ error , t);
			throw t;
		}
	}
	
	/**
	 * Checks if the current file already is equal in content with the renamed name. 
	 * @param file
	 * @return
	 * @throws Throwable 
	 */
	public static boolean isDuplicateFile(File file) throws Throwable {
		try {
			String alreadyRenamedFilePath = getRenamedFileName(file);
			File alreadyRenamedFile = new File(alreadyRenamedFilePath);
			boolean isDuplicateFile = areEqualFiles(file, alreadyRenamedFile);
			LOGGER.trace("Is duplicate File ? " + isDuplicateFile + " | File: " + file.getName());
			return isDuplicateFile;
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error checking if is a duplicate file"+ error , t);
			throw t;
		}
	}

	public static void delete(File file) throws Throwable {
		try {
			boolean deleted = file.delete();
			if(deleted) {
				LOGGER.debug("File deleted");
			}else {
				LOGGER.warn("Cannot delete file: " + file);				
			}
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error deleting file"+ error , t);
			throw t;
		}
		
	}

	public static boolean areEqualFiles(String filePath1, String filePath2) throws Throwable {
		try {
			File file1 = new File(filePath1);
			File file2 = new File(filePath2);
			boolean areEqual = areEqualFiles(file1, file2);
			return areEqual;			
		}catch(Throwable t) {
			String error = ". Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error deleting file"+ error , t);
			throw t;
		}
	}
		
	
}
