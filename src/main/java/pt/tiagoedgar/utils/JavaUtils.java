package pt.tiagoedgar.utils;

public class JavaUtils {

	public static boolean isEmptyOrNull(String value) {
		return (value == null || value.isEmpty());
	}
	
	public static boolean isNotEmptyOrNull(String value) {
		return !isEmptyOrNull(value);
	}
}
