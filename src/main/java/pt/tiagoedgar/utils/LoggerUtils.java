package pt.tiagoedgar.utils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggerUtils {
	
	private static final Logger LOGGER = Logger.getLogger(LoggerUtils.class);

	public static final String LOG_PROPERTY_FILE = "log4j.properties";
	
	/**
	 * Init the Log4j logger based on the properties file.
	 * If there is no property file, it will be created
	 * @throws Throwable 
	 *
	 */
	public static void init() throws Throwable {
		try {
			// Set up a simple configuration that logs on the console.
			BasicConfigurator.configure();
			
			// Load the configuration file
			String currentDirectory = AppUtils.getAppPath();
			String logFileConfigPath = currentDirectory+"/"+LOG_PROPERTY_FILE;
			File logFileConfig = new File(logFileConfigPath);
			
			// Create a default log file config if not exists
			if(!logFileConfig.exists()) {
				createLogConfigFile(logFileConfig);
			}
			PropertyConfigurator.configure(logFileConfigPath);
			LOGGER.trace("Log4j Started");
			
		}catch(Throwable t) {
			String error = " Error " + t.getMessage() + " | Cause: " + t.getCause() + " | Exception: " + t;
			LOGGER.error("Error on logger init."+ error , t);
			throw t;
		}
	}
	
	/**
	 * Create a default Log File Configuration
	 * @param logFileConfig
	 */
	public static void createLogConfigFile(File logFileConfig) {
		try {
			String defaultLogConfigs = 
					"log4j.rootLogger=trace, stdout, R\r\n" + 
							"\r\n" + 
							"log4j.appender.stdout=org.apache.log4j.ConsoleAppender\r\n" + 
							"log4j.appender.stdout.layout=org.apache.log4j.PatternLayout\r\n" + 
							"\r\n" + 
							"# Pattern to output the caller's file name and line number.\r\n" + 
							"log4j.appender.stdout.layout.ConversionPattern=%5p %d{yyyy-MM-dd HH:mm:ss,SSS} [%t] (%F:%4L) - %m%n\r\n" + 
							"\r\n" + 
							"log4j.appender.R=org.apache.log4j.RollingFileAppender\r\n" + 
							"log4j.appender.R.File=app.log\r\n" + 
							"\r\n" + 
							"log4j.appender.R.MaxFileSize=100MB\r\n" + 
							"# Keep one backup file\r\n" + 
							"log4j.appender.R.MaxBackupIndex=10\r\n" + 
							"\r\n" + 
							"log4j.appender.R.layout=org.apache.log4j.PatternLayout\r\n" + 
							"#log4j.appender.R.layout.ConversionPattern=%5p %t %c - %m%n\r\n" + 
							"log4j.appender.R.layout.ConversionPattern=%5p %d{yyyy-MM-dd HH:mm:ss,SSS} [%t] (%F:%4L) - %m%n";
			
			FileUtils.writeStringToFile(logFileConfig, defaultLogConfigs);
			LOGGER.info("Default configurations for Log4j created: " + logFileConfig.getName());
		}catch(Throwable t) {
			System.err.println("Error creating default log4j config file: " + t);
		}
	}

}
